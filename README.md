## java range：java vulnerability code

### Introduction

this project is a deliberately web application 

### Vulnerability Code

- [x] Commandi

- [ ] SQLi

- [ ] CORS

- [ ] SSRF

- [ ] CSRF

- [ ] CRLF Injection

- [ ] Deserialize


### Vulnerability Description
漏洞内容将同步在微信公众号上

![qrcode_for_gh_6afac1af7297_344](/images/qrcode_for_gh_6afac1af7297_344.jpg)



### Docker
支持docker

### Tomcat

### swagger ui
http://localhost:8090/swagger-ui.html

2020.5.13-5.20 搞定请求响应模型




